export interface BulletConstructorType {
  dx: number;
  dy: number;
  width: number;
  height: number;
  x: number;
  y: number;
}

export interface BulletType extends BulletConstructorType {
  draw: (context: CanvasRenderingContext2D) => void;
  update: (
    context: CanvasRenderingContext2D,
    canvas: HTMLCanvasElement
  ) => void;
}

export class Bullet {
  x: number;
  y: number;
  dx: BulletConstructorType["dx"];
  dy: BulletConstructorType["dy"];
  width: BulletConstructorType["width"];
  height: BulletConstructorType["height"];
  constructor({ x, y, dx, dy, width, height }: BulletConstructorType) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.width = width;
    this.height = height;
  }

  update(context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
    this.x += this.dx;
    this.y += this.dy;

    this.draw(context);
  }

  draw(context: CanvasRenderingContext2D) {
    context.fillStyle = "tomato";
    context.fillRect(this.x, this.y, this.width, this.height);
  }
}
