import { Box } from "@mui/system";
import React, { useEffect, useRef } from "react";
import { Bullet, BulletType } from "./bullet";
import { ship } from "./ship";

const Canvas = (): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const bullets: BulletType[] = [];

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas?.getContext("2d");

    let req: number;
    let fontSize = 10;
    function animate() {
      if (canvas != null && context != null) {
        context.fillStyle = "black";
        context.fillRect(0, 0, canvas.width, canvas.height);
        ship.update(context, canvas);

        bullets.forEach((bullet) => {
          if (
            bullet.x >= canvas.width / 2 - 35 &&
            bullet.x <= canvas.width / 2 + 24 &&
            bullet.y >= canvas.height / 2 &&
            bullet.y <= canvas.height / 2 + 10
          ) {
            bullet.x = -100;
            bullet.y = -100;
            bullet.dx = 0;
            bullet.dy = 0;
            bullet.update(context, canvas);
            console.log("hm");
            fontSize += 0.8;
          } else {
            bullet.update(context, canvas);
          }
        });

        context.fillStyle = "purple";
        context.font = `${fontSize}px serif`;
        context.textBaseline = "top";
        context.fillText(
          "HIGH PRICES",
          canvas.width / 2 - 35,
          canvas.height / 2,
          70
        );
      }
      if (fontSize > 30) {
      }
      req = requestAnimationFrame(animate);
    }

    animate();

    return () => {
      cancelAnimationFrame(req);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = () => {
    const bullet = new Bullet({
      x: ship.x,
      y: ship.y,
      dx: -Math.cos(ship.angle) * 2,
      dy: -Math.sin(ship.angle) * 2,
      width: 2,
      height: 2,
    });
    bullets.push(bullet);
  };

  return (
    <Box
      onClick={handleClick}
      sx={{ position: "relative" }}
      className="pageHeight"
    >
      <canvas
        style={{ display: "block", height: "100%", width: "100%" }}
        ref={canvasRef}
      ></canvas>
    </Box>
  );
};

export default Canvas;
