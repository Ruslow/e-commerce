import shipImg from "../../public/images/medfighter.png";

export interface ShipConstructorType {
  x: number;
  y: number;
  width: number;
  height: number;
  dx: number;
  dy: number;
  angle: number;
  radius: number;
}

interface ShipType extends ShipConstructorType {
  update: (
    context: CanvasRenderingContext2D,
    canvas: HTMLCanvasElement
  ) => void;
  draw: (context: CanvasRenderingContext2D) => void;
}

export class Ship {
  x: ShipConstructorType["x"];
  y: ShipConstructorType["y"];
  width: ShipConstructorType["width"];
  height: ShipConstructorType["height"];
  dx: ShipConstructorType["dx"];
  dy: ShipConstructorType["dy"];
  angle: ShipConstructorType["angle"];
  radius: ShipConstructorType["radius"];
  rotateAngle: number;
  constructor({
    x,
    y,
    width,
    height,
    dy,
    dx,
    angle,
    radius,
  }: ShipConstructorType) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.dx = dx;
    this.dy = dy;
    this.angle = angle;
    this.rotateAngle = 0;
    this.radius = radius;
  }

  update(context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
    this.x = Math.cos(this.angle) * this.radius;
    this.y = Math.sin(this.angle) * this.radius;
    this.draw(context, canvas);
    this.angle += 0.01;
  }
  draw(context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
    context.save();
    context.translate(canvas.width / 2, canvas.height / 2);
    context.rotate(this.angle);
    context.strokeStyle = "green";
    context.beginPath();
    context.moveTo(this.x, this.y);
    context.lineTo(this.x - 20, this.y);
    context.moveTo(this.x, this.y);
    context.lineTo(this.x - 10, this.y - 3);
    context.moveTo(this.x, this.y);
    context.lineTo(this.x - 10, this.y + 3);
    context.stroke();
    context.closePath();

    context.restore();
  }
}

export const ship = new Ship({
  x: 10,
  y: 10,
  width: 10,
  height: 10,
  dy: 1,
  dx: 1,
  angle: 0.01,
  radius: 60,
});
