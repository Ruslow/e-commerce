import { Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

const Footer = (): JSX.Element => {
  return (
    <Box
      bgcolor="primary.dark"
      sx={{
        textAlign: "center",
        padding: "2rem 0",
      }}
    >
      <Container>
        <Typography sx={{ fontSize: "1.7rem", letterSpacing: "1.5px" }}>
          TOP Clothing Store <br /> {new Date().getFullYear()} &#169; All Rights
          Reserved
        </Typography>
      </Container>
    </Box>
  );
};

export default Footer;
