import { Typography } from "@mui/material";
import React from "react";

const Heading = ({ children }: any) => {
  return (
    <Typography sx={{ marginBottom: "3rem", textAlign: "center" }} variant="h2">
      {children}
    </Typography>
  );
};

export default Heading;
