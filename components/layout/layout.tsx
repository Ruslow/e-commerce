import { ThemeProvider } from "@mui/material/styles";
import React, { useState } from "react";
import Footer from "../footer/footer";
import Navbar from "../navbar/navbar";
import SideBar from "../sideBar/sideBar";
import { theme } from "../../data/theme";

interface LayoutProps {
  children: React.ReactNode;
}

const Layout = ({ children }: LayoutProps): JSX.Element => {
  const [isSideBarShown, setIsSideBarShown] = useState(false);
  return (
    <ThemeProvider theme={theme}>
      <Navbar setIsSideBarShown={setIsSideBarShown} />
      {children}
      <SideBar
        isSideBarShown={isSideBarShown}
        setIsSideBarShown={setIsSideBarShown}
      />
      <Footer />
    </ThemeProvider>
  );
};

export default Layout;
