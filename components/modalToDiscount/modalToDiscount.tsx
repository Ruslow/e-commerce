import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Link from "next/link";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 450,
  bgcolor: "white",
  border: "2px solid #000",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
  textAlign: "center",
};

export default function ModalToDiscount() {
  const [isModalOpen, setIsModalOpen] = React.useState(true);
  return (
    <div>
      <Modal
        open={isModalOpen}
        onClose={() => {
          setIsModalOpen(false);
        }}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography
            sx={{ marginBottom: "1.3rem", letterSpacing: "1px" }}
            id="modal-modal-title"
            variant="h3"
            component="h2"
          >
            Do You Wanna Get a 10% Discount ?
          </Typography>
          <Typography
            sx={{ marginBottom: "1.3rem", letterSpacing: "1px" }}
            variant="h4"
            component="h2"
          >
            Play Our Game To Get That
          </Typography>
          <Link href={"/discount"} passHref>
            <Button
              sx={{ fontSize: "1.5rem", padding: "0.7rem 4rem" }}
              variant="contained"
              color="secondary"
            >
              Play
            </Button>
          </Link>
        </Box>
      </Modal>
    </div>
  );
}
