import {
  AppBar,
  Avatar,
  List,
  ListItem,
  Container,
  useTheme,
  useMediaQuery,
  ListItemText,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import Image from "next/image";
import { links } from "../../data/links";
import Link from "next/link";

interface NavbarTypes {
  setIsSideBarShown: React.Dispatch<React.SetStateAction<boolean>>;
}

const Navbar = ({ setIsSideBarShown }: NavbarTypes): JSX.Element => {
  const theme = useTheme();
  const smallQuery = useMediaQuery(theme.breakpoints.down("sm"));
  const midQuery = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <AppBar color="secondary" position="relative">
      <Container
        maxWidth="xl"
        sx={{
          color: "secondary.contrastText",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Image
          priority
          src="/images/logo.png"
          alt="TOP clothing store"
          width={60}
          height={60}
          layout="fixed"
        ></Image>
        {!smallQuery ? (
          <>
            <List
              sx={{
                display: "flex",
                fontSize: `${!midQuery ? "1.6rem" : "1.2rem"}`,
                letterSpacing: "1.8px",
              }}
            >
              {links.map((link) => {
                const { id, name, to } = link;
                return (
                  <ListItem key={id}>
                    <Link href={to}>
                      <a>{name}</a>
                    </Link>
                  </ListItem>
                );
              })}
            </List>
            <Avatar src="/images/me.jpg"></Avatar>
          </>
        ) : (
          <MenuIcon
            onClick={() => {
              setIsSideBarShown(true);
            }}
            style={{ fontSize: "3rem", cursor: "pointer" }}
          />
        )}
      </Container>
    </AppBar>
  );
};

export default Navbar;
