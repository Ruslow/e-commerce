import {
  Button,
  List,
  ListItem,
  Typography,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import { Box } from "@mui/system";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { goods } from "../../data/goods";
import Heading from "../heading/heading";
import ProductCard from "../productCard/productCard";

const NewProducts = () => {
  const theme = useTheme();
  const midQuery = useMediaQuery(theme.breakpoints.up("md"));
  return (
    <Box
      sx={{
        color: "secondary.contrastText",
        padding: "2rem 2rem",
        textAlign: "center",
      }}
      bgcolor="secondary.main"
    >
      <Heading>New Products</Heading>
      <List
        sx={{
          display: `${midQuery ? "flex" : ""}`,
          gap: "5rem",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {goods
          .filter((good) => good.newProd)
          .map((good) => {
            return <ProductCard key={good.id} restProps={good} />;
          })}
      </List>
      <Link href="/products" passHref>
        <Button
          sx={{ fontSize: midQuery ? "1.6rem" : "1.2rem" }}
          variant="contained"
        >
          All Products
        </Button>
      </Link>
    </Box>
  );
};

export default NewProducts;
