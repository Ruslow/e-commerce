import {
  Button,
  List,
  ListItem,
  Typography,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { Good } from "../../data/goods";

interface ProductCardProps {
  restProps: {
    name: Good["name"];
    price: Good["price"];
    images: Good["images"];
    id: Good["id"];
  };
}

const ProductCard = ({
  restProps: { name, price, images, id },
}: ProductCardProps) => {
  const theme = useTheme();
  const midQuery = useMediaQuery(theme.breakpoints.up("md"));

  console.log(id);

  return (
    <ListItem
      alignItems="center"
      sx={{
        backgroundColor: "secondary.light",
        marginBottom: "4rem",
        display: "block",
        textAlign: "center",
        fontSize: "1.5rem",
        padding: "2rem",
        borderRadius: "10px",
        flexBasis: "600px",
      }}
    >
      <Typography variant="body1" sx={{ fontSize: "2rem" }}>
        {name}
      </Typography>
      <Typography variant="body1" sx={{ fontSize: "1.8rem" }}>
        {price}$
      </Typography>
      <List
        sx={{
          display: "flex",
          gap: "2rem",
          justifyContent: "center",
        }}
      >
        {images.map((img) => {
          const { src, id, alt } = img;
          return (
            <Image
              key={id}
              width={midQuery ? "250" : "150"}
              height={midQuery ? "250" : "150"}
              src={src}
              alt={alt}
            />
          );
        })}
      </List>
      <Link href={`/products/${id}`} passHref>
        <Button
          sx={{
            marginTop: "1rem",
            fontSize: midQuery ? "1.4rem" : "1rem",
          }}
          variant="contained"
        >
          More Info
        </Button>
      </Link>
    </ListItem>
  );
};
export default ProductCard;
