import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
} from "@mui/material";
import { Box } from "@mui/system";
import { links, profileLinks } from "../../data/links";
import Link from "next/link";

interface SideBarProps {
  isSideBarShown: boolean;
  setIsSideBarShown: any;
}
const SideBar = ({
  isSideBarShown,
  setIsSideBarShown,
}: SideBarProps): JSX.Element => {
  return (
    <SwipeableDrawer
      anchor="right"
      onClose={() => {
        setIsSideBarShown(false);
      }}
      onOpen={() => {
        console.log("open");
      }}
      open={isSideBarShown}
    >
      <Box
        bgcolor="primary.dark"
        sx={{ height: "100%", width: "42vw" }}
        role="presentation"
      >
        <List>
          {links.map((link) => {
            const { id, name, to, Icon } = link;
            return (
              <Link href={to} key={id} passHref>
                <ListItem button>
                  <ListItemIcon>{<Icon />}</ListItemIcon>
                  <ListItemText primary={name} />
                </ListItem>
              </Link>
            );
          })}
        </List>
        <Divider />
        <List>
          {profileLinks.map((link, index) => {
            const { name, id, to, Icon } = link;
            return (
              <Link href={to} key={id} passHref>
                <ListItem button key={id}>
                  <ListItemIcon>{<Icon />}</ListItemIcon>
                  <ListItemText primary={name} />
                </ListItem>
              </Link>
            );
          })}
        </List>
      </Box>
    </SwipeableDrawer>
  );
};

export default SideBar;
