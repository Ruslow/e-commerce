export type Goods = Good[];

export interface Good {
  name: string;
  price: number;
  id: number;
  inStock: boolean;
  images: { src: string; id: number; alt: Good["name"] }[];
  newProd: boolean;
}

export const goods: Goods = [
  {
    name: "Nike Air Max Deluxe",
    price: 12000,
    id: 1,
    inStock: true,
    newProd: true,
    images: [
      {
        src: "/images/sneakers/nikeAirMaxDeluxe/1.jpg",
        id: 1,
        alt: "Nike Air Max Deluxe",
      },
      {
        src: "/images/sneakers/nikeAirMaxDeluxe/2.jpg",
        id: 2,
        alt: "Nike Air Max Deluxe",
      },
    ],
  },
  {
    name: "Comme Des Garcon x KAWS Shirt",
    price: 3000,
    id: 2,
    inStock: true,
    newProd: false,
    images: [
      {
        src: "/images/shirts/commeDesGarconsxKAWSShirt/1.jpg",
        id: 1,
        alt: "Comme Des Garcon x KAWS Shirt",
      },
      {
        src: "/images/shirts/commeDesGarconsxKAWSShirt/2.jpg",
        id: 2,
        alt: "Comme Des Garcon x KAWS Shirt",
      },
    ],
  },
  {
    name: "Uniqlo T-Shirt",
    price: 3200,
    id: 3,
    newProd: false,
    inStock: true,
    images: [
      {
        src: "/images/tshirts/uniqloT-Shirt/1.jpg",
        id: 1,
        alt: "Uniqlo T-Shirt",
      },
      {
        src: "/images/tshirts/uniqloT-Shirt/2.jpg",
        id: 2,
        alt: "Uniqlo T-Shirt",
      },
    ],
  },
  {
    name: "Yeezy Boost 700 MNV Honey Flux",
    price: 18000,
    id: 4,
    inStock: true,
    newProd: true,
    images: [
      {
        src: "/images/sneakers/yeezyBoost700MNVNHoneyFlux/1.jpg",
        id: 1,
        alt: "Yeezy Boost 700 MNV Honey Flux",
      },
      {
        src: "/images/sneakers/yeezyBoost700MNVNHoneyFlux/2.jpg",
        id: 2,
        alt: "Yeezy Boost 700 MNV Honey Flux",
      },
    ],
  },
  {
    name: "Guess T-Shirt",
    price: 4000,
    id: 5,
    newProd: true,
    inStock: true,
    images: [
      {
        src: "/images/tshirts/guessT-Shirt/1.jpg",
        id: 1,
        alt: "Guess T-Shirt",
      },
      {
        src: "/images/tshirts/guessT-Shirt/2.jpg",
        id: 2,
        alt: "Guess T-Shirt",
      },
    ],
  },
];
