import HomeIcon from "@mui/icons-material/Home";
import CheckroomIcon from "@mui/icons-material/Checkroom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import SettingsIcon from "@mui/icons-material/Settings";
import LogoutIcon from "@mui/icons-material/Logout";
import InfoIcon from "@mui/icons-material/Info";

export const links = [
  { id: 1, name: "Home", to: "/", Icon: HomeIcon },
  { id: 2, name: "About", to: "/about", Icon: InfoIcon },
  { id: 3, name: "Products", to: "/products", Icon: CheckroomIcon },
];

export const profileLinks = [
  { id: 1, name: "Cart", to: "/cart", Icon: ShoppingCartIcon },
  { id: 2, name: "Settings", to: "/settings", Icon: SettingsIcon },
  { id: 3, name: "Log out", to: "/logout", Icon: LogoutIcon },
];
