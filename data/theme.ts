import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#66FCf1",
      dark: "#45A29E",
      contrastText: "#0B0C10",
    },
    secondary: {
      main: "#0B0C10",
      light: "#1F2833",
      contrastText: "#66FCf1",
    },
  },
});
