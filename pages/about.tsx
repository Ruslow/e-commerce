import { Box } from "@mui/system";
import { NextPage } from "next";
import React from "react";
import Layout from "../components/layout/layout";

const About: NextPage = (): JSX.Element => {
  return (
    <Layout>
      <Box className="pageHeight"></Box>
    </Layout>
  );
};

export default About;
