import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import { NextPage } from "next";
import Head from "next/head";
import React, { useEffect, useRef } from "react";
import Canvas from "../components/canvas/canvas";
import Layout from "../components/layout/layout";

const About: NextPage = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>Get Discount - TOP Clothes</title>
      </Head>
      <Layout>
        <Canvas />
      </Layout>
    </>
  );
};

export default About;
