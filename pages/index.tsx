import type { NextPage } from "next";
import Head from "next/head";
import Header from "../components/header/header";
import Layout from "../components/layout/layout";
import ModalToDiscount from "../components/modalToDiscount/modalToDiscount";
import NewProducts from "../components/newProducts/newProducts";

const Home: NextPage = (): JSX.Element => {
  return (
    <>
      <Head>
        <title>Home - TOP Clothes</title>
      </Head>
      <Layout>
        <Header />
        <NewProducts />
        <ModalToDiscount />
      </Layout>
    </>
  );
};

export default Home;
