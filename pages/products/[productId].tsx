import { Box } from "@mui/system";
import Head from "next/head";
import { useRouter } from "next/router";
import React from "react";
import Heading from "../../components/heading/heading";
import Layout from "../../components/layout/layout";
import ProductCard from "../../components/productCard/productCard";
import { goods } from "../../data/goods";

const ProductPage = () => {
  const router = useRouter();
  const { productId } = router.query;

  if (productId != null) {
    var product = goods.find((good) => good.id === +productId);
  }
  const getProductName = product != null && `${product.name}`;

  return (
    <>
      <Head>
        <title>{getProductName}</title>
      </Head>
      <Layout>
        <Box
          sx={{ padding: "2rem 1rem", color: "primary.main" }}
          bgcolor="secondary.dark"
          className="pageHeight"
        >
          <Heading>{getProductName}</Heading>
          {product != null && <h2>Hello</h2>}
        </Box>
      </Layout>
    </>
  );
};

export default ProductPage;
