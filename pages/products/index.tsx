import { List, useMediaQuery, useTheme } from "@mui/material";
import { Box } from "@mui/system";
import { NextPage } from "next";
import Head from "next/head";
import React from "react";
import Heading from "../../components/heading/heading";
import Layout from "../../components/layout/layout";
import ProductCard from "../../components/productCard/productCard";
import { goods } from "../../data/goods";

const Products: NextPage = (): JSX.Element => {
  const theme = useTheme();
  const midQuery = useMediaQuery(theme.breakpoints.up("md"));
  return (
    <>
      <Head>
        <title>Products - TOP Clothes</title>
      </Head>
      <Layout>
        <Box
          sx={{ padding: "2rem 1rem", color: "primary.main" }}
          bgcolor="secondary.dark"
          className="pageHeight"
        >
          <Heading>Products</Heading>
          <List
            sx={{
              display: `${midQuery ? "flex" : ""}`,
              gap: "5rem",
              flexWrap: "wrap",
              justifyContent: "center",
            }}
          >
            {goods.map((good) => {
              return <ProductCard key={good.id} restProps={good} />;
            })}
          </List>
        </Box>
      </Layout>
    </>
  );
};

export default Products;
